import matplotlib.pyplot as plt
import numpy as np


class gr1:
    def __init__(self):
        pass

    def checkPoint(self, x, y):
        # y = x+1
        # y = x^2
        a = (y >= x+1)
        b = (y >= x*x)
        if a and b:
            print("Сектор 4")
        elif a and (not b) and x > 0:
            print("Сектор 2")
        elif a and (not b) and x < 0:
            print("Сектор 5")
        elif (not a) and (not b):
            print("Сектор 1")
        elif (not a) and b:
            print("Сектор 3")

    def printGraphic(self):
        xpoints = np.arange(-3., 3., 0.2)
        plt.plot(xpoints, xpoints+1, 'r--', xpoints, xpoints*xpoints)
        plt.show()

