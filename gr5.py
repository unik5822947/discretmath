class gr5:
    def __init__(self):
        pass

    def checkPoint(self, x, y):
        # x^2+y^2=1
        # y = x
        # y = -x+1
        a = (abs(x)*abs(x)+abs(y)*abs(y) <= 1)
        b = (abs(y) > abs(x))
        c = (abs(y) > -abs(x)+1)
        if a and c and b:
            print("Попадает в А")
        elif not b and not c:
            print("Попадает в В")
        else:
            print("Никуда не попадает")

    #def printGraph(self):
