import math


class gr2:
    def __init__(self):
        pass

    def checkPoint(self, x, y):
        # y = -x+1
        # y = x^2
        # y = log2(x)
        a = (y >= -x+1)
        b = (y >= x*x)
        c = True
        if x > 0:
            c = (y >= math.log2(x))
        if not a and not c:
            print("Sektor 1")
        elif a and not c:
            print("Sektor 2")
        elif a and c and not b and x > 0:
            print("Sektor 3")
        elif a and not b and x < 0:
            print("Sektor 6")
        elif a and b:
            print("Sektor 4")
        elif not a and b:
            print("Sektor 5")
        if x > 0:
            if not a and not b and c:
                print("Sektor 7")
        else:
            if not a and not b:
                print("Sektor 7")
