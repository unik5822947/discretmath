from gr1 import gr1
from gr2 import gr2
from gr3 import gr3
from gr4 import gr4
from gr5 import gr5


def check1(x, y):
    a = gr1()
    a.printGraphic()
    a.checkPoint(x, y)


def check2(x, y):
    a = gr2()
    a.checkPoint(x, y)


def check3(x, y):
    a = gr3()
    a.checkPoint(x, y)


def check4(x, y):
    a = gr4()
    a.checkPoint(x, y)


def check5(x, y):
    a = gr5()
    a.checkPoint(x, y)


if __name__ == '__main__':
    check1(float(input("Введите Х ")), float(input("Введите Y ")))


