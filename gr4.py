class gr4:
    def __init__(self):
        pass

    def checkPoint(self, x, y):
        # y = -x+3
        # y = -3x+3
        # y = -(1/3)x+1
        a = (abs(y) < -abs(x)+3)
        b = (abs(y) < -3*abs(x)+3)
        c = (abs(y) < -(1/3)*abs(x)+1)
        if a and not b and not c:
            print(f"Точка ({x}; {y}) попадает в А")
        elif b and c:
            print(f"Точка ({x}; {y}) попадает в B")
        else:
            print(f"Точка ({x}; {y}) никуда не попадает")